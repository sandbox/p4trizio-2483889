<?php

/**
 * @file
 * Default rule configurations for Commerce Email Notify.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_email_notify_default_rules_configuration() {

  $rules = array();
  
  // Add a reaction rule to send order e-mail upon order updating completion.
  $rule = rules_reaction_rule();

  $rule->label = t('Commerce email notify (HTML)');
  $rule->active = TRUE;
  
  $rule
    ->event('commerce_email_notify_order_form_submit')
    ->condition('data_is', array(
      'data:select' => 'commerce-order:commerce-email-notify',
      'op' => '==',
      'value' => '1',
    ))
    ->action('variable_email_mail', array(
      'to:select' => 'commerce-order:mail',
      'variable' => 'commerce_email_notify_[mail_part]',
      'language' => 'commerce-order:owner:language',
    ))
    ->action('drupal_message', array(
      'message' => 'Message has been sent to customer.',
    ));
    
  $rule->weight = 4;

  $rules['commerce_email_notify'] = $rule;
  


  return $rules;
}
