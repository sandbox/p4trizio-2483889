<?php

/**
 * Implements hook_variable_info().
 */
function commerce_email_notify_variable_info($options) {
  $variable['commerce_email_notify_[mail_part]'] = array(
    'title' => t('Email to customer'),
    'type' => 'mail_html',
    'description' => t('Template for the email sent out to the customer.'),
    'children' => array(
      'commerce_email_notify_subject' => array(
        'default' => 'News: order [commerce-order:order-number] at [site:name]',
      ),
      'commerce_email_notify_body' => array(
        'default' => '<p>There are news for your order [commerce-order:order-number] at [site:name].</p><p>[commerce-order:commerce-email-log]</p><p>You can view the status of your current order at: <a href="[site:url]user/[commerce-order:uid]/orders/[commerce-order:order-id]">[site:url]user/[commerce-order:uid]/orders/[commerce-order:order-id]</a></p><p>Please contact us if you have any questions about your order.</p>'
      ),
    ),
    'group' => 'commerce_email',
  );

  return $variable;
}  
