<?php

/**
 * @file
 * Defines additional tokens for order email.
 */

/**
 * Implements hook_token_info().
 */
function commerce_email_notify_token_info() {
  $info['tokens']['commerce-order']['commerce-email-notify'] = array(
    'name' => t('Commerce email notify'),
    'description' => t("Order has a flag to notify user"),
  );
  $info['tokens']['commerce-order']['commerce-email-log'] = array(
    'name' => t('Commerce email log'),
    'description' => t("Last order revision log"),
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function commerce_email_notify_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $url_options = array('absolute' => TRUE);

  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
    $language_code = $options['language']->language;
  }
  else {
    $language_code = NULL;
  }

  $sanitize = !empty($options['sanitize']);

  $replacements = array();

  if ($type == 'commerce-order' && !empty($data['commerce-order'])) {
    $order = $data['commerce-order'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'commerce-email-notify':
          $replacements[$original] = $order->notify;
          break;
        case 'commerce-email-log':
          $replacements[$original] = $order->log;
          break;
      }
    }
  }
  return $replacements;
}
